FROM debian:11-slim

RUN dpkg --add-architecture i386 && apt update && apt install -y --no-install-recommends \
 lib32gcc-s1 lib32stdc++6 lib32tinfo6 lib32z1 ca-certificates locales && apt clean && \
 rm -rf /var/lib/apt/lists/* && \
 localedef -i en_US -c -f UTF-8 -A /usr/share/locale/locale.alias en_US.UTF-8

ENV LANG en_US.UTF-8

RUN ln -s $(readlink -f /lib32/libtinfo.so.6) /lib32/libtinfo.so.5

CMD ["bash"]