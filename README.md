# srcds-docker

Image using as runtime for srcds with [pufferpanel](https://github.com/pufferpanel/pufferpanel). Not contains steamcmd

### Example template
```json
{
  "name": "css-docker",
  "display": "Counter Strike: Source (Docker)",
  "type": "srcds",
  "install": [
    {
      "type": "download",
      "files": [
        "http://media.steampowered.com/installer/steamcmd_linux.tar.gz"
      ]
    },
    {
      "type": "mkdir",
      "target": "steamcmd"
    },
    {
      "type": "command",
      "commands": [
        "tar --no-same-owner -xzf steamcmd_linux.tar.gz -C steamcmd",
        "steamcmd/steamcmd.sh +force_install_dir /pufferpanel +login anonymous +app_update 232330 +quit",
        "mkdir -p .steam/sdk32",
        "cp steamcmd/linux32/steamclient.so .steam/sdk32/steamclient.so",
        "rm steamcmd_linux.tar.gz"
      ]
    }
  ],
  "run": {
    "stop": "exit",
    "command": "./srcds_run -game cstrike -ip ${ip} -port ${port} +clientport ${clientport} +tv_port ${tvport} +map ${map} -maxplayers ${maxplayers} -norestart",
    "workingDirectory": "",
    "pre": [],
    "post": [],
    "environmentVars": {}
  },
    "data": {
      "map": {
      "value": "$2000$",
      "required": true,
      "desc": "Map to load by default",
      "display": "Map",
      "internal": false,
      "userEdit": true
    },
    "maxplayers": {
      "value": "18",
      "required": true,
      "desc": "Max players allowed on server",
      "display": "Max Players",
      "internal": false,
      "type": "integer"
    },
    "ip": {
      "value": "0.0.0.0",
      "required": true,
      "desc": "What IP to bind the server to",
      "display": "IP",
      "internal": false
    },
    "port": {
      "value": "27015",
      "required": true,
      "desc": "What port to bind the server to",
      "display": "Port",
      "internal": false,
      "type": "integer"
    },
    "clientport": {
      "value": "27005",
      "required": true,
      "desc": "What port to bind the clientport to",
      "display": "Client Port",
      "internal": false,
      "type": "integer"
    },
    "tvport": {
      "value": "28015",
      "required": true,
      "desc": "What port to bind the SourceTV to",
      "display": "SourceTV Port",
      "internal": false,
      "type": "integer"
    }
  },
  "environment": {
    "image": "idk1703/srcds",
    "type": "docker"
  },
  "supportedEnvironments": [
    {
      "image": "idk1703/srcds",
      "type": "docker"
    }
  ],
  "requirements": {
    "arch": "amd64"
  }
}
```